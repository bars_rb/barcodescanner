package com.example.mmp.barcodescanner;

/**
 * Created by mmp on 05.11.2015.
 */
public class Points {
    
    String date;
    String name;
    boolean sent;


    Points(String _date, String _name, boolean _sent) {
        date = _date;
        name = _name;
        sent = _sent;
    }
}