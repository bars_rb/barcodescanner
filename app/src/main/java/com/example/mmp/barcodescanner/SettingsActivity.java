package com.example.mmp.barcodescanner;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SettingsActivity extends AppCompatActivity {

    String ean_list_uri = "http://gps.aromata.ru/ean_list.php";
    int serverResponseCode;
    ProgressBar pg, pgh;
    TextView tvResult;
    int count;
    DBHelper dbHelper;
    String EAN, title, nomen;
    ContentValues cv;
    SQLiteDatabase db;

    String apkurl = "http://gps.aromata.ru/updates/barcodescanner_current.apk";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        pg = (ProgressBar)findViewById(R.id.progressBar);
        pgh = (ProgressBar) findViewById(R.id.progressBar2);
        tvResult = (TextView)findViewById(R.id.tvResult);
    }

    public void btnGetEANOnClick(View view){
        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();
        cv = new ContentValues();

        new Thread(new Runnable() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(SettingsActivity.this, "Получаем список ШК. Ждите.", Toast.LENGTH_SHORT).show();
                        pg.setVisibility(ProgressBar.VISIBLE);
                    }
                });

                URL url;
                HttpURLConnection urlConnection = null;


                try {
                    url = new URL(ean_list_uri);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String line=reader.readLine().trim();
                    String[] parts;
                    count=Integer.parseInt(line);
                    if(count>0)db.delete("EAN",null,null);
                    int t_pos;
                    while ((line = reader.readLine()) != null) {
                        count--;
                        parts = line.split(";");
                        cv.put("EAN", parts[0]);
                        cv.put("nomen", parts[1]);
                        cv.put("title", parts[2]);

                        // вставляем запись
                        db.insert("EAN", null, cv);
                        cv.clear();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                pgh.setProgress((2000-count)/20);
                            }
                        });
                    }
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        urlConnection.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace(); //If you want further info on failure...
                    }
                }

                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(SettingsActivity.this, count + "", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(SettingsActivity.this, "Готово" ,Toast.LENGTH_SHORT).show();
                        pg.setVisibility(ProgressBar.INVISIBLE);
                        pgh.setProgress(0);
                    }
                });
                //finish();
            }
        }).start();

    }

    public void btnUpdateOnClick(View view) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    URL url = new URL(apkurl);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    c.setRequestMethod("GET");
                    c.setDoOutput(true);
                    c.connect();

                    String PATH = Environment.getExternalStorageDirectory() + "/download/";
                    File file = new File(PATH);
                    file.mkdirs();
                    File outputFile = new File(file, "barcodescanner_current.apk");
                    FileOutputStream fos = new FileOutputStream(outputFile);

                    InputStream is = c.getInputStream();

                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);
                    }
                    fos.close();
                    is.close();//till here, it works fine - .apk is download to my sdcard in download file

            /*Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                    .setData(Uri.parse(PATH+"app.apk"))
                    .setType("application/android.com.app");
            startActivity(promptInstall);//installation is not working */
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + "barcodescanner_current.apk")), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Update error!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();
    }
}
