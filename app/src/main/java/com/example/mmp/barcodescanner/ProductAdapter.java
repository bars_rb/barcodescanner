package com.example.mmp.barcodescanner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by mmp on 26.10.2015.
 */
public class ProductAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Product> objects;

    ProductAdapter(Context context, ArrayList<Product> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    //@Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    //@Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    //@Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
   // @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.product_row, parent, false);
        }

        Product p = getProduct(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.EAN)).setText(p.EAN);
        ((TextView) view.findViewById(R.id.title)).setText(p.title);
        ((TextView) view.findViewById(R.id.count)).setText(Integer.toString(p.count));

        return view;
    }

    // товар по позиции
    Product getProduct(int position) {
        return ((Product) getItem(position));
    }


}
