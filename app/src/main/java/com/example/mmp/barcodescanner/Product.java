package com.example.mmp.barcodescanner;

/**
 * Created by mmp on 26.10.2015.
 */
public class Product {

    String EAN;
    String nomen;
    String title;
    int count;


    Product(String _EAN, String _nomen, String _title, int _count) {
        EAN = _EAN;
        nomen = _nomen;
        title = _title;
        count = _count;
    }
}
