package com.example.mmp.barcodescanner;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BarCodeScan extends AppCompatActivity {
    private Button btnScan;
    private Button btnAdd;
    private TextView tvResult;
    private EditText etCount;
    DBHelper dbHelper;
    String lastEAN, lastTitle, lastNomen;
    String point;

    private View del_view;

    ArrayList<Product> products = new ArrayList<Product>();
    ProductAdapter productAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_scan);
        btnScan = (Button)findViewById(R.id.btnGetPlan);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        tvResult = (TextView)findViewById(R.id.tvTitle);
        etCount = (EditText) findViewById(R.id.etCount);
        dbHelper = new DBHelper(this);

        //npCount.setOrientation(LinearLayout.HORIZONTAL);
    }

    public void btnScanOnClick(View v){
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null && scanningResult.getContents()!=null) {
            //ContentValues cv = new ContentValues();
            String scanContent = scanningResult.getContents();
            //Cursor c = null;
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            //cv.put("name", scanContent);
            //String selection = "EAN = ";
            //String [] selectionArgs = new String[] { scanContent};
            //scanContent="2000000044477";
            //c = db.query("EAN", null, selection, selectionArgs, null, null, null);
            Cursor c = db.rawQuery("SELECT * FROM EAN WHERE TRIM(EAN) = '"+scanContent.trim()+"'", null);

            //Results
            if (c != null) {
                tvResult.setText("ШТРИХ-КОД НЕ НАЙДЕН");
                if (c.moveToFirst()) {
                    {
                        tvResult.setText("Позиция: " + c.getString(2));
                        lastEAN = c.getString(0);
                        lastNomen = c.getString(1);
                        lastTitle = c.getString(2);
                        btnAdd.setEnabled(true);
                    } while (c.moveToNext());
                }
                c.close();
            } else
                tvResult.setText("ШТРИХ-КОД НЕ НАЙДЕН");
            //Log.d(LOG_TAG, "Cursor is null");
            dbHelper.close();

            //Results end



            //tvResult.setText("CONTENT: " + scanContent);
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }


    }

    public void btnAddOnClick(View v) {
        int count;
        if (etCount.getText().toString().matches("")) count=1;
        else count=Integer.parseInt(etCount.getText().toString());

        if(!lastTitle.matches("")) {

            products.add(new Product(lastEAN, lastNomen, lastTitle, count));
            productAdapter = new ProductAdapter(this, products);

            // настраиваем список
            ListView lvMain = (ListView) findViewById(R.id.listView2);
            lvMain.setAdapter(productAdapter);
            etCount.setText("");
            lastEAN="";
            lastTitle="";
            lastNomen="";
            btnAdd.setEnabled(false);
        }
    }

    public void btnDelOnClick(View view) {

        del_view = view;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("");
        builder.setMessage("Удалить эту запись?");


        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView lvMain = (ListView) findViewById(R.id.listView2);
                int pos = lvMain.getPositionForView((View) del_view.getParent());
                Product pr = (Product) lvMain.getItemAtPosition(lvMain.getCheckedItemPosition());
                products.remove(pos);
                productAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });


        builder.setNegativeButton("NO", null);

        AlertDialog alert = builder.create();
        alert.show();




    }

    public void btnDoneOnClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("");
        builder.setMessage("Введите название точки.");

        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        input.setFocusableInTouchMode(true);
        input.requestFocus();


        builder.setPositiveButton("Готово", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


                point = input.getText().toString();
                if (point.matches("")) {
                    Toast.makeText(getApplicationContext(), "Вы не ввели название!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (products.size() == 0) {
                    Toast.makeText(getApplicationContext(), "Вы ничего не отсканировали.", Toast.LENGTH_SHORT).show();
                    return;
                }

                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues cv = new ContentValues();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());


                cv.put("name", point);
                cv.put("date", currentDateandTime);
                // вставляем запись и получаем ее ID
                long rowID = db.insert("points", null, cv);
                //Log.d(LOG_TAG, "row inserted, ID = " + rowID);

                for (int i = 0; i < products.size(); i++) {
                    cv.clear();
                    cv.put("point", rowID);
                    cv.put("nomen", products.get(i).nomen);
                    cv.put("count", products.get(i).count);
                    db.insert("remains", null, cv);
                }

                Toast.makeText(getApplicationContext(), "Список сохранён " + point, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                db.close();
                finish();
            }
        });


        builder.setNegativeButton("Отмена", null);

        AlertDialog alert = builder.create();
        alert.show();
    }


}
