package com.example.mmp.barcodescanner;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Button btnSendRemains;

    DBHelper dbHelper;
    final String targetURL="http://gps.aromata.ru/save_remains.php";
    String deviceID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSendRemains = (Button)findViewById(R.id.btnSendRemains);

        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        deviceID = mngr.getDeviceId();
        if(deviceID==null)deviceID="000000000000000";

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        checkSent();

        ListView lv = (ListView)findViewById(R.id.lvPoints);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                View pointView=arg1;
                ListView lvMain = (ListView) findViewById(R.id.listView2);
                //int pos = lvMain.getPositionForView((View) pointView.getParent());
                int pos = arg0.getPositionForView((View) pointView.getParent());
            }
        });

    }

    @Override
    protected  void onResume()
    {
        super.onResume();
        checkSent();
    }

    @Override
    protected  void onStart()
    {
        super.onStart();
        checkSent();
    }

    private void checkSent(){
        ArrayList<Points> points = new ArrayList<Points>();
        PointsAdapter pointsAdapter;
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT date, name, SUM(sent) FROM points JOIN remains ON points.id=remains.point GROUP BY name, date ORDER BY date DESC;", null);
        String date="";
        String name="";
        boolean sent=false;

        boolean allsent=true;

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    date = c.getString(0);
                    name = c.getString(1);
                    if(c.getInt(2)==0)sent = true; else {sent=false;allsent=false;}
                    points.add(new Points(date, name, sent));
                } while (c.moveToNext());
            }

            c.close();
        }
        dbHelper.close();
        pointsAdapter = new PointsAdapter(this, points);

        // настраиваем список
        ListView lvMain = (ListView) findViewById(R.id.lvPoints);
        lvMain.setAdapter(pointsAdapter);

        if(!allsent) {
            btnSendRemains.setEnabled(true);
            btnSendRemains.setText("Отправить");
        }
    }

   /* public void lvPointsOnClick (View v){
        View pointView=v;
        ListView lvMain = (ListView) findViewById(R.id.listView2);
        int pos = lvMain.getPositionForView((View) pointView.getParent());
    }*/

    public void btnSendRemainsOnClick(View v){
        String params="";
        String res="";
        String rid="";
        ContentValues values = new ContentValues();
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT remains.id as rid, remains.nomen as nomen, remains.count as count, points.id as point_id, points.name as point_name, points.date as date FROM remains JOIN points ON remains.point = points.id WHERE sent = 1;", null);
        //Results
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                        rid = c.getString(0);
                        String nomen="",count="",point_id="",point_name="",date="";
                        try {
                            nomen = URLEncoder.encode(c.getString(1), "UTF-8");
                            count = URLEncoder.encode(String.valueOf(c.getInt(2)), "UTF-8");
                            point_id=URLEncoder.encode(String.valueOf(c.getInt(3)), "UTF-8");
                            point_name=URLEncoder.encode(c.getString(4), "UTF-8");
                            date = URLEncoder.encode(c.getString(5), "UTF-8");
                        } catch (IOException ex) {Toast.makeText(MainActivity.this, ex.toString(), Toast.LENGTH_LONG).show();}
                        params = "dev_id=" + deviceID + "&nomen=" + nomen; //nomen
                        params = params + "&count=" + count;  //count
                        params = params + "&point_id=" + point_id; //point_id
                        params = params + "&point_name=" + point_name;
                        params = params + "&date=" +date;
                        res = excutePost(params).trim();
                        //res="777";
                        if (res.equals("777")) {
                            values.put("sent", "0");
                            int updCount = db.update("remains", values, "id = ?", new String[]{rid});
                            //Toast.makeText(getApplicationContext(), String.valueOf(updCount), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getApplicationContext(), "Ошибка отправки данных", Toast.LENGTH_SHORT).show();

                }while (c.moveToNext());
            }

            c.close();
        }


        Cursor c2 = db.rawQuery("SELECT * FROM remains WHERE sent = 1;", null);

        //Results
        if (c2.getCount() == 0) {
            btnSendRemains.setEnabled(false);
            btnSendRemains.setText("Всё отправленно");
            Toast.makeText(getApplicationContext(), "Всё отправленно!", Toast.LENGTH_SHORT).show();
        }
        c2.close();
        dbHelper.close();
        checkSent();
    }

    public void btnScanOnClick(View v){
        Intent intent = new Intent(this, BarCodeScan.class);
        startActivity(intent);
    }

    public void btnSetOnClick(View v){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public String excutePost(String urlParameters)
    {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(this.targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
        }
    }

}
