package com.example.mmp.barcodescanner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by mmp on 26.10.2015.
 */
public class PointsAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Points> objects;

    PointsAdapter(Context context, ArrayList<Points> points) {
        ctx = context;
        objects = points;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    //@Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    //@Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    //@Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    // @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.point_row, parent, false);
        }

        Points p = getPoints(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        ((TextView) view.findViewById(R.id.tvName)).setText(p.name);
        ((TextView) view.findViewById(R.id.tvDate)).setText(p.date);
        if(p.sent){((ImageView)view.findViewById(R.id.imgSent)).setImageResource(R.drawable.ok);}
        else{((ImageView)view.findViewById(R.id.imgSent)).setImageResource(R.drawable.no);}

        return view;
    }

    // товар по позиции
    Points getPoints(int position) {
        return ((Points) getItem(position));
    }


}
