package com.example.mmp.barcodescanner;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DBHelper extends SQLiteOpenHelper {

    //final int db_version=2;

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "barcode", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Log.d("1", "--- onCreate database ---");
        // создаем таблицу с полями
        db.execSQL("CREATE TABLE EAN (EAN STRING (13)  PRIMARY KEY UNIQUE,NOMEN STRING (16),TITLE STRING (128) );");
        db.execSQL("CREATE TABLE points (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(64), date VARCHAR(15));");
        db.execSQL("CREATE TABLE remains (id INTEGER PRIMARY KEY AUTOINCREMENT, point INTEGER, nomen STRING (16), count INTEGER, sent  BOOLEAN DEFAULT (1) );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*if(oldVersion==1 && newVersion==2){
            db.execSQL("CREATE TABLE points (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(64), date VARCHAR(15));");
            db.execSQL("CREATE TABLE remains (id INTEGER PRIMARY KEY AUTOINCREMENT, point INTEGER, nomen STRING (16), count INTEGER, sent  BOOLEAN DEFAULT (1) );");
        }*/

    }
}